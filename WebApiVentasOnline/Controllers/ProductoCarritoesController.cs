﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiVentasOnline.Models;

namespace WebApiVentasOnline.Controllers
{
    public class ProductoCarritoesController : ApiController
    {
        private WebApiVentasOnlineContext db = new WebApiVentasOnlineContext();

        // GET: api/ProductoCarritoes
        public IQueryable<ProductoCarrito> GetProductoCarritoes()
        {
            return db.ProductoCarritoes;
        }

        // GET: api/ProductoCarritoes/5
        [ResponseType(typeof(ProductoCarrito))]
        public async Task<IHttpActionResult> GetProductoCarrito(int id)
        {
            ProductoCarrito productoCarrito = await db.ProductoCarritoes.FindAsync(id);
            if (productoCarrito == null)
            {
                return NotFound();
            }

            return Ok(productoCarrito);
        }

        // PUT: api/ProductoCarritoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProductoCarrito(int id, ProductoCarrito productoCarrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productoCarrito.idProducto)
            {
                return BadRequest();
            }

            db.Entry(productoCarrito).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductoCarritoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProductoCarritoes
        [ResponseType(typeof(ProductoCarrito))]
        public async Task<IHttpActionResult> PostProductoCarrito(ProductoCarrito productoCarrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ProductoCarritoes.Add(productoCarrito);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductoCarritoExists(productoCarrito.idProducto))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = productoCarrito.idProducto }, productoCarrito);
        }

        // DELETE: api/ProductoCarritoes/5
        [ResponseType(typeof(ProductoCarrito))]
        public async Task<IHttpActionResult> DeleteProductoCarrito(int id)
        {
            ProductoCarrito productoCarrito = await db.ProductoCarritoes.FindAsync(id);
            if (productoCarrito == null)
            {
                return NotFound();
            }

            db.ProductoCarritoes.Remove(productoCarrito);
            await db.SaveChangesAsync();

            return Ok(productoCarrito);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductoCarritoExists(int id)
        {
            return db.ProductoCarritoes.Count(e => e.idProducto == id) > 0;
        }
    }
}