﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiVentasOnline.Models;

namespace WebApiVentasOnline.Controllers
{
    public class CarritoesController : ApiController
    {
        private WebApiVentasOnlineContext db = new WebApiVentasOnlineContext();

        // GET: api/Carritoes
        public IQueryable<Carrito> GetCarritoes()
        {
            return db.Carritoes;
        }

        // GET: api/Carritoes/5
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> GetCarrito(int id)
        {
            Carrito carrito = await db.Carritoes.FindAsync(id);
            if (carrito == null)
            {
                return NotFound();
            }

            return Ok(carrito);
        }

        // PUT: api/Carritoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCarrito(int id, Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != carrito.idCarrito)
            {
                return BadRequest();
            }

            db.Entry(carrito).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarritoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Carritoes
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> PostCarrito(Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Carritoes.Add(carrito);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = carrito.idCarrito }, carrito);
        }

        // DELETE: api/Carritoes/5
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> DeleteCarrito(int id)
        {
            Carrito carrito = await db.Carritoes.FindAsync(id);
            if (carrito == null)
            {
                return NotFound();
            }

            db.Carritoes.Remove(carrito);
            await db.SaveChangesAsync();

            return Ok(carrito);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CarritoExists(int id)
        {
            return db.Carritoes.Count(e => e.idCarrito == id) > 0;
        }
    }
}