namespace WebApiVentasOnline.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carritoes",
                c => new
                    {
                        idCarrito = c.Int(nullable: false, identity: true),
                        idUsuario = c.Int(nullable: false),
                        precioTotal = c.Int(nullable: false),
                        estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.idCarrito)
                .ForeignKey("dbo.Usuarios", t => t.idUsuario, cascadeDelete: true)
                .Index(t => t.idUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        idUsuario = c.Int(nullable: false, identity: true),
                        username = c.String(nullable: false),
                        pass = c.String(nullable: false),
                        nombre = c.String(nullable: false),
                        rol = c.Boolean(nullable: false),
                        tarjetaCredito = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idUsuario);
            
            CreateTable(
                "dbo.ProductoCarritoes",
                c => new
                    {
                        idProducto = c.Int(nullable: false),
                        idCarrito = c.Int(nullable: false),
                        precioUnitario = c.Int(nullable: false),
                        cantidadProducto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.idProducto, t.idCarrito })
                .ForeignKey("dbo.Carritoes", t => t.idCarrito, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.idProducto, cascadeDelete: true)
                .Index(t => t.idProducto)
                .Index(t => t.idCarrito);
            
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        idProducto = c.Int(nullable: false, identity: true),
                        nombreProducto = c.String(nullable: false),
                        descripcion = c.String(nullable: false),
                        rutaImagen = c.String(nullable: false),
                        precio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idProducto);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductoCarritoes", "idProducto", "dbo.Productoes");
            DropForeignKey("dbo.ProductoCarritoes", "idCarrito", "dbo.Carritoes");
            DropForeignKey("dbo.Carritoes", "idUsuario", "dbo.Usuarios");
            DropIndex("dbo.ProductoCarritoes", new[] { "idCarrito" });
            DropIndex("dbo.ProductoCarritoes", new[] { "idProducto" });
            DropIndex("dbo.Carritoes", new[] { "idUsuario" });
            DropTable("dbo.Productoes");
            DropTable("dbo.ProductoCarritoes");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Carritoes");
        }
    }
}
