namespace WebApiVentasOnline.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApiVentasOnline.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApiVentasOnline.Models.WebApiVentasOnlineContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApiVentasOnline.Models.WebApiVentasOnlineContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,    
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Usuarios.AddOrUpdate(
                  u => u.idUsuario,
                  new Usuario {idUsuario=1, nombre="Juan Perez", username="juan", pass="juan", tarjetaCredito="123", rol=true},
                  new Usuario {idUsuario=2, nombre="Juan Perez", username="juana", pass="juana", tarjetaCredito="123", rol=false}
                );
            context.Productoes.AddOrUpdate(
                p => p.idProducto,
                new Producto { idProducto=1, nombreProducto="Manzana", descripcion="producto rojo", precio=1, rutaImagen="manzana.jpg"}
                );
        }
    }
}
