﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiVentasOnline.Models
{
    public class Carrito
    {
        [Required,Key]
        public int idCarrito { get; set; }
        [Required]
        public int idUsuario { get; set; }
        [ForeignKey("idUsuario")]
        public Usuario usuario { get; set; }
        [Required]
        public int precioTotal { get; set; }
        [Required]
        public bool estado { get; set; }
    }
}