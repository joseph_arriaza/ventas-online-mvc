﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Usuario
    {
        [Key]
        public int idUsuario { get; set; }
        [Required]
        public String username { get; set; }
        [Required]
        public String pass { get; set; }
        [Required]
        public String nombre { get; set; }
        [Required]
        public bool rol { get; set; }
        [Required, CreditCard]
        public String tarjetaCredito { get; set; }
    }
}