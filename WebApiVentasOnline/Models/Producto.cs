﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiVentasOnline.Models
{
    public class Producto
    {
        [Key]
        public int idProducto { get; set; }
        [Required]
        public String nombreProducto { get; set; }
        [Required]
        public String descripcion { get; set; }
        [Required]
        public String rutaImagen { get; set; }
        [Required]
        public int precio{ get; set; }
    }
}