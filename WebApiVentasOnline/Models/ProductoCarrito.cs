﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiVentasOnline.Models
{
    public class ProductoCarrito
    {
        [Key, Column(Order = 0)]
        public int idProducto { get; set; }
        [ForeignKey("idProducto")]
        public Producto producto { get; set; }
        [Key, Column(Order = 1)]
        public int idCarrito { get; set; }
        [ForeignKey("idCarrito")]
        public Carrito carrito { get; set; }
        [Required]
        public int precioUnitario { get; set; }
        [Required]
        public int cantidadProducto { get; set; }
    }
}