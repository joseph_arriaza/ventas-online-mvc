﻿var ViewIndex = function () {
    var self = this;

    //USUARIO INCIADO
    self.userLogged = ko.observable();
    self.rolUser = ko.observable();
    //USUARIO EDITAR
    self.usuarioEditar = ko.observable();
    //ARRAYS OBSERVABLES
    self.listaUsers = ko.observableArray();
    self.listaProductos = ko.observableArray();
    self.carritoDelUsuario = ko.observableArray();
    self.listaCarritosComprados = ko.observableArray();
    self.listaCarritosProductosComprados = ko.observableArray();
    self.comprandoCarrito = ko.observable();
    //ERROR
    self.error = ko.observable();
    //DATOS AL INICIAR SESION
    self.userName = ko.observable();
    self.userPassword = ko.observable();

    //PRODUCTO A EDITAR
    self.productoEditar = ko.observable();
    
    self.estadoCarrito = ko.observable();
    self.estado = ko.observable();
    self.loginError = ko.observable();
    //URIS

    var usersUri = '/api/Usuarios';
    var productsUri = '/api/Productoes/';
    var carritosUri = '/api/Carritoes/';
    var productsCarritosUri = '/api/ProductoCarritoes/';

    //VARIABLES GLOBALES
    var cont = 0;
    var precioTotal = 0;
    //AJAXHELPER

    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        self.loginError('');
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if(data!='Login'){
                self.error(errorThrown);
            }
        });
    }
    //USUARIOS

    //VARIABLES PARA AGREGAR USUARIO

    self.newUsuario = {
        username: ko.observable(),
        pass: ko.observable(),
        nombre: ko.observable(),
        rol: ko.observable(),
        tarjetaCredito: ko.observable()
    }

    //REGISTRAR UN NUEVO USUARIO 

    self.crearUsuario = function (formElement) {
        var usuario = {
            username: self.newUsuario.username(),
            pass: self.newUsuario.pass(),
            nombre: self.newUsuario.nombre(),
            rol: false,
            tarjetaCredito: 4111111111111111
        };
        ajaxHelper(usersUri, 'POST', usuario).done(function (item) {
            //SE ACTUALIZA LA LISTA DE LOS USUARIOS
            self.listaUsers.push(item);
            self.newUsuario.pass("");
            self.newUsuario.nombre("");
            self.newUsuario.username("");
            self.userLogged(item);
            self.rolUser(item.rol);
            localStorage.setItem("logUser", JSON.stringify(item));
        });
    }

    //REGISTRAR UN NUEVO ADMINISTRADOR

    self.crearAdmin = function (formElement) {
        var admin = {
            username: self.newUsuario.username(),
            pass: self.newUsuario.pass(),
            nombre: self.newUsuario.nombre(),
            rol: true,
            tarjetaCredito: 4111111111111111
        };
        ajaxHelper(usersUri, 'POST', admin).done(function (item) {
            //SE ACTUALIZA LA LISTA DE LOS USUARIOS
            self.listaUsers.push(item);
            self.newUsuario.pass("");
            self.newUsuario.nombre("");
            self.newUsuario.username("");
            alert("Administrador agregado correctamente!");
        });
    }

    //EDITAR UN USUARIO

    
    self.getUserEdit = function (item) {
        self.estado('');
        ajaxHelper(usersUri +'/'+ item.idUsuario, 'GET').done(function (data) {
            self.usuarioEditar(data);
        });
    }

    //CAMPOS PARA EDITAR UN USUARIO

    self.editUsuarios = {
        nombre: "",
        pass: "",
        tarjetaCredito: 4111111111111111
    }

    //EDITAR EL USUARIO

    self.editUsuario = function (formElement) {
        self.usuarioEditar().tarjetaCredito = 4111111111111111;
        var newUsuarioEdit = self.usuarioEditar();
        ajaxHelper(usersUri + '/' + self.usuarioEditar().idUsuario, 'PUT', newUsuarioEdit).done(function (item) {   
            self.listaUsers.replace(self.usuarioEditar(),item);
            getAllUsers();
            self.estado("Usuario editado");
        });
    }

    //ACTUALIZAR O LLENAR LISTA DE USUARIOS

    function getAllUsers() {
        ajaxHelper(usersUri, 'GET').done(function (data) {
            self.listaUsers(data);
        });
    }

    //INICIAR SESION CON EL USUARIO

    self.login = function () {
        var action = false;
        ko.utils.arrayForEach(self.listaUsers(), function (usuario) {
            if (usuario.username == self.userName() && usuario.pass == self.userPassword()) {
                self.userLogged(usuario);
                self.loginError('');
                action = true;
                self.rolUser(usuario.rol);
                localStorage.setItem("logUser", JSON.stringify(usuario));
                self.userName("");
            } else if (action == false) {
                //alert("action: "+ action);
                self.loginError('El usuario y/o contraseña son incorrectos!');
            }
            //alert(usuario.idUsuario);
        });
        self.userPassword("");
    }


    //PRODUCTOS

    //VARIABLES PARA NUEVO PRODUCTO


    

    self.newProductoes = {
        nombreProducto: ko.observable(),
        descripcion: ko.observable(),
        rutaImagen: ko.observable(),
        precio: ko.observable()
    }

    //FUCION QUE VERIFICA EL ROL

    self.verificarRol = function () {
        return self.userLogged.rol();
    }
    
    //REGISTRAR UN PRODUCTO

    self.crearProducto = function (formElement) {
        var files = $("#inputFile").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            data.append("file" + i, files[i]);
        }
        $.ajax({
            type: "POST",
            url: "/api/Docfile",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                if (result) {
                    $("#inputFile").val('');
                    location.href = "Productos";
                }
            }
        });
        var producto = {
            nombreProducto: self.newProductoes.nombreProducto(),
            descripcion: self.newProductoes.descripcion(),
            rutaImagen: self.newProductoes.rutaImagen(),
            precio: self.newProductoes.precio()
        };
        var cadena = producto.rutaImagen.split("\\");
        producto.rutaImagen = cadena[cadena.length - 1];
        ajaxHelper('/api/Docfile', 'POST', producto.rutaImagen).done(function (item) {
            alert("Ruta lista");
        });
        ajaxHelper(productsUri, 'POST', producto).done(function (item) {
            //SE ACTUALIZA LA LISTA DE LOS PRODUCTOS
            self.listaProductos.push(item);
            self.newProductoes.nombreProducto(""); self.newProductoes.descripcion(""); self.newProductoes.precio(""); self.newProductoes.rutaImagen("");
        });
    }


    //LLENAR EL CONTACTO A EDITAR

    self.getProductoEdit = function (item) {
        self.estado('');
        ajaxHelper(productsUri + item.idProducto, 'GET').done(function (data) {
            self.productoEditar(data);
            //alert(JSON.stringify(self.productoEditar()));
        });
    }

    //DATOS PARA EDITAR UN PRODUCTO

    self.editProductoes = {
        nombreProducto: "",
        descripcion: "",
        precio: 0
    };

    //EDITAR UN PRODUCTO

    self.editProducto = function (formElement) {
        var newProductAdd = self.productoEditar();
        ajaxHelper(productsUri + self.productoEditar().idProducto, 'PUT', newProductAdd).done(function (item) {
            self.listaProductos.replace(self.productoEditar, item);
            getAllProductoes();
            self.estado("Producto editado");
        });
    }


    //ELIMINAR PRODUCTOS

    self.getProductDelete = function (item) {
        ajaxHelper(productsUri + item.idProducto, 'DELETE').done(function (data) {
            self.listaProductos.remove(item);
        });
    }

   //LLENAR O ACTUALIAZAR LISTA DE PRODUCTOS

    function getAllProductoes(){
        ajaxHelper(productsUri, 'GET').done(function (data) {       
            self.listaProductos(data);
        });
    }


    //ACCIONES DEL CARRITO

    //AGREGA UN ARTICULO AL CARRITO

    self.buyArticle = function (item) {
        var verificador = false;
        self.estadoCarrito("");
        if(localStorage.getItem("carrito")!=null){
            self.carritoDelUsuario(JSON.parse(localStorage.getItem("carrito")));
        }
        if (self.comprandoCarrito() == null) {
            self.comprandoCarrito(0);
        }
        var newProductoCart = {
            idProducto: item.idProducto,
            producto: item,
            idCarrito: cont,
            carrito: ko.observable(),
            precioUnitario: item.precio,
            cantidadProducto: 1
        }
        precioTotal = (newProductoCart.precioUnitario) * (newProductoCart.cantidadProducto);
        if(self.carritoDelUsuario()==null){
            self.carritoDelUsuario = ko.observableArray();
        }
        ko.utils.arrayForEach(self.carritoDelUsuario(), function (carrito) {
            if (carrito.idProducto == item.idProducto) {
                carrito.cantidadProducto += 1;
                self.estadoCarrito("Ahora tiene " + carrito.cantidadProducto + " " + carrito.producto.nombreProducto);
                self.comprandoCarrito((carrito.precioUnitario) + self.comprandoCarrito()*1);
                verificador = true;
            }
        });
        if (!verificador) {
            self.carritoDelUsuario.push(newProductoCart);
            self.comprandoCarrito((newProductoCart.precioUnitario) + self.comprandoCarrito());
            self.estadoCarrito("Compra agregada al carrito");
        }
        localStorage.setItem("carrito", JSON.stringify(self.carritoDelUsuario()));
        localStorage.setItem("precioCarro", self.comprandoCarrito());
    }

    //ELIMINAMOS UNO DE CANTIDAD DEL CARRITO
    
    self.discountArticle = function (item) {
        self.estadoCarrito("");
        ko.utils.arrayForEach(self.carritoDelUsuario(), function (carritoQuitar) {
            if (carritoQuitar == item) {
                carritoQuitar.cantidadProducto = item.cantidadProducto - 1;
                if(carritoQuitar.cantidadProducto<=0){
                    self.carritoDelUsuario.remove(carritoQuitar);
                    self.comprandoCarrito(self.comprandoCarrito() - carritoQuitar.precioUnitario);
                    self.estadoCarrito("Se ha eliminado el producto del carrito!");
                } else {
                    self.estadoCarrito("Ahora tiene " + carritoQuitar.cantidadProducto + "  " + carritoQuitar.producto.nombreProducto);
                    self.comprandoCarrito(self.comprandoCarrito() - carritoQuitar.precioUnitario);
                }
            }
        });
        localStorage.setItem("precioCarro", self.comprandoCarrito());
        localStorage.setItem("carrito", JSON.stringify(self.carritoDelUsuario()));
        self.carritoDelUsuario(JSON.parse(localStorage.getItem("carrito")));
    }

    //AGREGAR PRODUCTO AL CARRITO
    self.addCarrito = function () {
        if (self.carritoDelUsuario().length > 0) {
            var carritoAdd = {
                idCarrito: 0,
                idUsuario: self.userLogged().idUsuario,
                precioTotal: self.comprandoCarrito(),
                estado: true
            }
            ajaxHelper(carritosUri, 'POST', carritoAdd).done(function (item) {
                self.listaCarritosComprados.push(item);
                var idCarro = 0;
                ko.utils.arrayForEach(self.listaCarritosComprados(), function (item) {
                    idCarro = item.idCarrito;
                });
                ko.utils.arrayForEach(self.carritoDelUsuario(), function (vista) {
                    var carritoProductoAdd = {
                        idProducto: vista.producto.idProducto,
                        idCarrito: idCarro,
                        precioUnitario: vista.precioUnitario,
                        cantidadProducto: vista.cantidadProducto
                    }
                    ajaxHelper(productsCarritosUri, 'POST', carritoProductoAdd).done(function (item) {
                        self.listaCarritosProductosComprados.push(item);
                        localStorage.setItem("carrito",null);
                        localStorage.setItem("precioCarro", null);
                        self.carritoDelUsuario(null);
                        self.comprandoCarrito(null);
                        self.estadoCarrito("Ha comprado el carrito exitosamente!");
                    });
                });
            });
        } else {
            self.estadoCarrito("No tienes ningun articulo a comprar!");
        }
    }

    //CIERRA LA SESION DEL USUARIO

    self.logout = function () {
        self.userLogged("");
        self.rolUser("");
        localStorage.setItem("logUser", null);
        localStorage.setItem("carrito", null);
        localStorage.setItem("precioCarro", null);
        localStorage.clear();
    }

   


    // Fetch the initial data.
    getAllUsers();
    getAllProductoes();

    $('.self.userLogged').ready(function () {
        self.userLogged(JSON.parse(localStorage.getItem("logUser")));
        self.rolUser(JSON.parse(localStorage.getItem("logUser")).rol);
    });

    $('.self.carritoDelUsuario').ready(function () {
        self.carritoDelUsuario(JSON.parse(localStorage.getItem("carrito")));
    });

    $('.self.comprandoCarrito').ready(function () {
        self.comprandoCarrito(JSON.parse(localStorage.getItem("precioCarro")));
    });

};

ko.applyBindings(new ViewIndex());